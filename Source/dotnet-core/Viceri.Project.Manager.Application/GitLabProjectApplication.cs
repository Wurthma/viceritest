﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Application.Extensions;
using System.Security.Cryptography;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Application
{
    public class GitLabProjectApplication : IGitLabProjectApplication
    {
        private readonly IGitLabProjectService _gitLabProjectService;

        public GitLabProjectApplication(IGitLabProjectService gitLabProjectService)
        {
            _gitLabProjectService = gitLabProjectService;
        }

        public async Task<IEnumerable<GitLabProjectViewModel>> AllAsync()
        {
            IEnumerable<GitLabProject> projectList = await _gitLabProjectService.AllAsync();
            return projectList.EnumerableTo<GitLabProjectViewModel>();
        }

        public async Task<bool> ExistAsync(int id)
        {
            return await _gitLabProjectService.ExistAsync(id);
        }

        public async Task<GitLabProjectViewModel> GetAsync(int id)
        {
            GitLabProject project = await _gitLabProjectService.GetAsync(id);
            var projectVw = project.MapTo<GitLabProjectViewModel>();
            projectVw.IsImported = true;
            return projectVw;
        }

        public async Task<bool> SaveProjectAsync(GitLabProjectViewModel projectVw)
        {
            var project = projectVw.MapTo<GitLabProject>();
            return await _gitLabProjectService.SaveProjectAsync(project);
        }

        public async Task<bool> UpdateProjectAsync(GitLabProjectViewModel projectVw)
        {
            GitLabProject project = projectVw.MapTo<GitLabProject>();
            return await _gitLabProjectService.UpdateProjectAsync(project);
        }
    }
}

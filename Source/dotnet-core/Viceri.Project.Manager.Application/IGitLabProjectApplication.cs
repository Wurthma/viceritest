﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application
{
    public interface IGitLabProjectApplication
    {
        Task<bool> SaveProjectAsync(GitLabProjectViewModel projectVw);
        Task<GitLabProjectViewModel> GetAsync(int id);
        Task<bool> ExistAsync(int id);
        Task<IEnumerable<GitLabProjectViewModel>> AllAsync();
        Task<bool> UpdateProjectAsync(GitLabProjectViewModel projectVw);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Project.Manager.Application.Mappings
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Entities.User, ViewModels.UserViewModel>();

            CreateMap<Domain.Entities.GitLabProject, ViewModels.GitLabProjectViewModel>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.name_with_namespace, opt => opt.MapFrom(src => src.NameWithNameSpace))
                .ForMember(dest => dest.path, opt => opt.MapFrom(src => src.Path))
                .ForMember(dest => dest.path_with_namespace, opt => opt.MapFrom(src => src.PathWithNameSpace))
                .ForMember(dest => dest.created_at, opt => opt.MapFrom(src => src.CreateAt.ToLongDateString()))
                .ForMember(dest => dest.import_status, opt => opt.MapFrom(src => src.ImportStatus))
                .ForMember(dest => dest.IsImported, opt => opt.Ignore());

            CreateMap<ViewModels.GitLabProjectViewModel, Domain.Entities.GitLabProject>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.description))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.name))
                .ForMember(dest => dest.NameWithNameSpace, opt => opt.MapFrom(src => src.name_with_namespace))
                .ForMember(dest => dest.Path, opt => opt.MapFrom(src => src.path))
                .ForMember(dest => dest.PathWithNameSpace, opt => opt.MapFrom(src => src.path_with_namespace))
                .ForMember(dest => dest.CreateAt, opt => opt.MapFrom(src => Convert.ToDateTime(src.created_at)))
                .ForMember(dest => dest.ImportStatus, opt => opt.MapFrom(src => src.import_status));
        }
    }
}

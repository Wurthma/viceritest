﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Project.Manager.Application.ViewModels
{
    public class GitLabProjectViewModel
    {
        public int id { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string name_with_namespace { get; set; }
        public string path { get; set; }
        public string path_with_namespace { get; set; }
        public string created_at { get; set; }
        public string import_status { get; set; }
        public bool IsImported { get; set; }
    }
}

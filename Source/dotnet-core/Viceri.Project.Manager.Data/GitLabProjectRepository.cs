﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class GitLabProjectRepository : IGitLabProjectRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public GitLabProjectRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GitLabProject>> AllAsync()
        {
            return await _context.GitLabProject.ToListAsync();
        }

        public async Task<bool> ExistAsync(int id)
        {
            return await _context.GitLabProject.CountAsync(p => p.Id == id) > 0;
        }

        public async Task<GitLabProject> GetAsync(int id)
        {
            return await _context.GitLabProject.SingleOrDefaultAsync(q => q.Id == id);
        }

        public async Task<bool> SaveProjectAsync(GitLabProject project)
        {
            await _context.GitLabProject.AddAsync(project);
            return await _context.SaveChangesAsync() == 1;
        }

        public async Task<bool> UpdateProjectAsync(GitLabProject project)
        {
            _context.GitLabProject.Update(project);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}

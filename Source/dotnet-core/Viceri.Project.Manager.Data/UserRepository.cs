﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public UserRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public async Task<User> Login(string username, string password)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Username == username && u.Password == password);
        }
    }
}

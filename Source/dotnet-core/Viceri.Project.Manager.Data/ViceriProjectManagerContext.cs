﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Data.EntityTypeConfigurations;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data
{
    public class ViceriProjectManagerContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<GitLabProject> GitLabProject { get; set; }

        public ViceriProjectManagerContext(DbContextOptions<ViceriProjectManagerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
            modelBuilder.Entity<GitLabProject>().HasKey(k => k.Id);
            modelBuilder.Entity<GitLabProject>().Property(p => p.Id).ValueGeneratedNever();
        }
    }
}

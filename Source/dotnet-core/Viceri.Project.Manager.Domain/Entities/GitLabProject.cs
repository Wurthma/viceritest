﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Project.Manager.Domain.Entities
{
    public class GitLabProject
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string NameWithNameSpace { get; set; }
        public string Path { get; set; }
        public string PathWithNameSpace { get; set; }
        public DateTime CreateAt { get; set; }
        public string ImportStatus { get; set; }
    }
}

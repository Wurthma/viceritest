﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IGitLabProjectService
    {
        Task<bool> SaveProjectAsync(GitLabProject project);
        Task<GitLabProject> GetAsync(int id);
        Task<bool> ExistAsync(int id);
        Task<IEnumerable<GitLabProject>> AllAsync();
        Task<bool> UpdateProjectAsync(GitLabProject project);
    }
}

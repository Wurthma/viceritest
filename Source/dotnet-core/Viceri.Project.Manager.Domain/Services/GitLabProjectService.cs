﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class GitLabProjectService : IGitLabProjectService
    {
        private readonly IGitLabProjectRepository _gitLabProjectRepository;

        public GitLabProjectService(IGitLabProjectRepository gitLabProjectRepository)
        {
            _gitLabProjectRepository = gitLabProjectRepository;
        }

        public async Task<IEnumerable<GitLabProject>> AllAsync()
        {
            return await _gitLabProjectRepository.AllAsync();
        }

        public async Task<bool> ExistAsync(int id)
        {
            return await _gitLabProjectRepository.ExistAsync(id);
        }

        public async Task<GitLabProject> GetAsync(int id)
        {
            return await _gitLabProjectRepository.GetAsync(id);
        }

        public async Task<bool> SaveProjectAsync(GitLabProject project)
        {
            return await _gitLabProjectRepository.SaveProjectAsync(project);
        }

        public async Task<bool> UpdateProjectAsync(GitLabProject project)
        {
            return await _gitLabProjectRepository.UpdateProjectAsync(project);
        }
    }
}

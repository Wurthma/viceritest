﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;
using Xunit;

namespace Viceri.Project.Manager.Tests.Application
{
    public class GitLabProjectApplicationTest : UnitTestBase
    {
        private readonly Mock<IGitLabProjectService> _projectServiceMock;

        public GitLabProjectApplicationTest()
        {
            _projectServiceMock = new Mock<IGitLabProjectService>();
        }

        [Fact]
        public async Task AllAsync_method_should_be_executed_with_success()
        {
            IEnumerable<GitLabProject> projectList = new List<GitLabProject>{
                new GitLabProject {
                    Id = 1,
                    Description = "Project ID 1 descripition test",
                    Name = "ProjectId_1",
                    NameWithNameSpace = "ProjectId_1 With Name Space",
                    Path = "Test / ProjectId_1",
                    PathWithNameSpace = "Test / ProjectId_1 / With Name Space",
                    CreateAt = DateTime.Now,
                    ImportStatus = "complete"
                },
                new GitLabProject {
                    Id = 2,
                    Description = "Project ID 2 descripition test",
                    Name = "ProjectId_2",
                    NameWithNameSpace = "ProjectId_2 With Name Space",
                    Path = "Test / ProjectId_2",
                    PathWithNameSpace = "Test / ProjectId_2 / With Name Space",
                    CreateAt = DateTime.Now,
                    ImportStatus = "complete"
                }
            };

            _projectServiceMock.Setup(x => x.AllAsync()).Returns(Task.FromResult(projectList));

            var gitLabProjectApplication = new GitLabProjectApplication(_projectServiceMock.Object);

            var listProjectResult = await gitLabProjectApplication.AllAsync();

            Assert.NotNull(listProjectResult);
        }

        [Fact]
        public async Task ExistAsync_method_should_return_true()
        {
            int expectedId = 123;

            _projectServiceMock.Setup(x => x.ExistAsync(expectedId)).Returns(Task.FromResult(true));

            var gitLabProjectApplication = new GitLabProjectApplication(_projectServiceMock.Object);

            var exists = await gitLabProjectApplication.ExistAsync(expectedId);

            Assert.True(exists);
        }

        [Fact]
        public async Task ExistAsync_method_should_return_false()
        {
            int notExpectedId = 321;

            _projectServiceMock.Setup(x => x.ExistAsync(notExpectedId)).Returns(Task.FromResult(false));

            var gitLabProjectApplication = new GitLabProjectApplication(_projectServiceMock.Object);

            var exists = await gitLabProjectApplication.ExistAsync(notExpectedId);

            Assert.False(exists);
        }

        [Fact]
        public async Task GetAsync_method_should_be_executed_with_success()
        {
            int id = 5;
            GitLabProject project = new GitLabProject {
                Id = id,
                Description = "Project ID 1 descripition test",
                Name = "ProjectId_1",
                NameWithNameSpace = "ProjectId_1 With Name Space",
                Path = "Test / ProjectId_1",
                PathWithNameSpace = "Test / ProjectId_1 / With Name Space",
                CreateAt = DateTime.Now,
                ImportStatus = "complete"
            };

            _projectServiceMock.Setup(x => x.GetAsync(id)).Returns(Task.FromResult(project));

            var gitLabProjectApplication = new GitLabProjectApplication(_projectServiceMock.Object);

            var projectResult = await gitLabProjectApplication.GetAsync(id);

            Assert.NotNull(projectResult);
            Assert.Equal(project.Id, projectResult.id);
            Assert.Equal(project.Description, projectResult.description);
            Assert.Equal(project.Name, projectResult.name);
            Assert.Equal(project.NameWithNameSpace, projectResult.name_with_namespace);
            Assert.Equal(project.Path, projectResult.path);
            Assert.Equal(project.PathWithNameSpace, projectResult.path_with_namespace);
            Assert.Equal(project.ImportStatus, projectResult.import_status);
            Assert.True(projectResult.IsImported);
            Assert.IsType<GitLabProject>(project);
            Assert.IsType<GitLabProjectViewModel>(projectResult);
        }
    }
}

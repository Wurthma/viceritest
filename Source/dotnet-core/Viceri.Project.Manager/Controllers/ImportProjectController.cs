﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Application.Mappings;
using Viceri.Project.Manager.Application;
using Microsoft.AspNetCore.Authorization;

namespace Viceri.Project.Manager.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]    
    public class ImportProjectController : Controller
    {
        public IConfiguration Configuration { get; set; }
        private readonly IGitLabProjectApplication _gitLabProjectApplication;
        private HttpClient httpClient;
        private string gitLabURI;
        private string gitLabToken;

        public ImportProjectController(IConfiguration configuration, IGitLabProjectApplication gitLabProjectApplication)
        {
            _gitLabProjectApplication = gitLabProjectApplication;
            httpClient = new HttpClient();
            Configuration = configuration;
            gitLabToken = Configuration["Authentication:GitLab:Token"];
            gitLabURI = Configuration["GitLabURI"];
        }

        [HttpGet]
        [Route("Save/{id}")]
        public async Task<IActionResult> Put(int id)
        {
            GitLabProjectViewModel gitLabProjectVw = new GitLabProjectViewModel();

            HttpResponseMessage response = await httpClient.GetAsync($"{gitLabURI}/{id}/import/?private_token={gitLabToken}");
            if (response.IsSuccessStatusCode)
            {
                gitLabProjectVw = await response.Content.ReadAsAsync<GitLabProjectViewModel>();
                if (await _gitLabProjectApplication.ExistAsync(id))
                {
                    gitLabProjectVw.IsImported = await _gitLabProjectApplication.UpdateProjectAsync(gitLabProjectVw);
                }
                else if (!await IsForked(id))
                {
                    gitLabProjectVw.IsImported = await _gitLabProjectApplication.SaveProjectAsync(gitLabProjectVw);
                }
                
                if (gitLabProjectVw.IsImported)
                {
                    gitLabProjectVw = await _gitLabProjectApplication.GetAsync(id);
                }
            }
            return Json(gitLabProjectVw);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            GitLabProjectViewModel gitLabProjectVw = await _gitLabProjectApplication.GetAsync(id);
            return Json(gitLabProjectVw);
        }

        [HttpGet]
        [Route("All")]
        public async Task<IActionResult> All()
        {
            IEnumerable<GitLabProjectViewModel> gitLabProjectVwList = await _gitLabProjectApplication.AllAsync();
            return Json(gitLabProjectVwList);
        }

        private async Task<bool> IsForked(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync($"{gitLabURI}/{id}/?private_token={gitLabToken}");
            if (response.IsSuccessStatusCode)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var test = jsonResult.Contains("forked_from_project");
                return test;
            }
            else
            {
                return false;
            }
        }
    }
}
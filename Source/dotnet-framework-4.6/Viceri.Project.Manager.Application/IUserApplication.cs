﻿using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application
{
    public interface IUserApplication
    {

        UserViewModel Login(string username, string password);
    }
}
﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Data;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Services;

namespace Viceri.Project.Manager.IoC
{
    public static class IoCConfiguration
    {
        public static void Configure(Container container)
        {
            ConfigureDomain(container);
            ConfigureData(container);
            ConfigureApplication(container);
        }

        private static void ConfigureApplication(Container container)
        {
            container.Register<IUserApplication, UserApplication>(Lifestyle.Scoped);
        }

        private static void ConfigureData(Container container)
        {
            container.Register<ViceriProjectManagerContext>(Lifestyle.Scoped);
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
        }

        private static void ConfigureDomain(Container services)
        {
            services.Register<IUserService, UserService>(Lifestyle.Scoped);
        }
    }
}

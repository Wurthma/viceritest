﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;
using Xunit;

namespace Viceri.Project.Manager.Tests.Application
{
    public class UserApplicationTest : UnitTestBase
    {
        private readonly Mock<IUserService> _userServiceMock;

        public UserApplicationTest()
        {
            _userServiceMock = new Mock<IUserService>();
        }
        [Fact]
        public void Login_method_should_throw_ArgumentNullException_for_username()
        {
            var userApplication = new UserApplication(_userServiceMock.Object);

            Assert.Throws<ArgumentNullException>("username", () =>
            {
                userApplication.Login(null, "anypassword");
            });
        }
        [Fact]
        public void Login_method_should_throw_ArgumentNullException_for_password()
        {
            var userApplication = new UserApplication(_userServiceMock.Object);
            Assert.Throws<ArgumentNullException>("password", () =>
            {
                userApplication.Login("anyuser", null);
            });
        }

        [Fact]
        public void  Login_method_should_be_executed_with_success()
        {
            var expectedUser = "anyuser";
            var expectedHash = "5323a259d7eca7e317248d9cb94217838059973d31fc57219864f57bf3ee28bd";//Sha256 for 'anypassword'

            _userServiceMock.Setup(x => x.Login(expectedUser, expectedHash)).Returns(new User
            {
                Id = 1,
                Email = "anyuser@host.com",
                IsDeleted= false,
                Password = expectedHash,
                Username = expectedUser
            });

            var userApplication = new UserApplication(_userServiceMock.Object);

            var loginResult = userApplication.Login("anyuser", "anypassword");

            Assert.NotNull(loginResult);
            Assert.Equal("anyuser@host.com", loginResult.Email);
            Assert.Equal(expectedUser, loginResult.Username);
            Assert.False(loginResult.IsDeleted);
            Assert.Equal(1, loginResult.Id);

        }
    }
}

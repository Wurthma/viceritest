﻿using System.Net;
using System.Web.Http;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Filters;

namespace Viceri.Project.Manager.Controllers
{
    public class TokenController : ApiController
    {
        private readonly IUserApplication _userApplication;

        public TokenController(IUserApplication userApplication)
        {
            _userApplication = userApplication;

        }

        [JwtAuthentication]
        public IHttpActionResult Get()
        {
            return Ok(new {
                message = "Yeah"
            });
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult Post([FromBody] TokenRequest tokenRequest)
        {
            if (tokenRequest == null)
                return BadRequest();
            var user = _userApplication.Login(tokenRequest.Username, tokenRequest.Password);
            if (user != null)
            {
                var token = JwtManager.GenerateToken(tokenRequest.Username);
                return Ok(new
                {
                    token
                });
            }
            else
                return BadRequest("Invalid username or password.");
        }
    }

    public class TokenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
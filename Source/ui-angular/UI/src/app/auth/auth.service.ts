import { Injectable } from '@angular/core';
import { AppConstants } from '../app.contants';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token: string;

  constructor(
    private http: HttpClient
  ) { }


  public sigin(username: string, password: string): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(environment.ApiEndpoint + "token", {
      username: username,
      password: password
    });
  }
  
  public isAuthorized(): boolean {
    let authorized = this.token != null;
    if (!authorized) {
      let token = localStorage.getItem(AppConstants.TokenLocalStorageKey);
      if (token != null && token != "" && token != undefined) {
        this.token = token;
        authorized = this.token != null;
      }
    }
    return authorized;
  }

  public saveToken(token: string) : void {
    localStorage.setItem(AppConstants.TokenLocalStorageKey, token);
    this.token = token;
  }
}

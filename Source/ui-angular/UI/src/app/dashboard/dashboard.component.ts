import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Project } from 'src/models/project.models';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  projectList : Project[];
  title : string;
  
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.GetAllProjects();
  }

  GetAllProjects(){
    this.http.get<Project[]>(environment.ApiEndpoint + "ImportProject/All")
      .subscribe(
        projects => this.projectList = projects
      );
      this.title = "Projeto importado com sucesso!";
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Project } from 'src/models/project.models';

@Component({
  selector: 'app-import-project',
  templateUrl: './import-project.component.html',
  styleUrls: ['./import-project.component.css']
})
export class ImportProjectComponent implements OnInit {

  project : Project;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  public ImportProject(id: number){
    this.http.get<Project>(environment.ApiEndpoint + "ImportProject/Save/" + id)
      .subscribe(
        project => this.project = project
      );
  }
}

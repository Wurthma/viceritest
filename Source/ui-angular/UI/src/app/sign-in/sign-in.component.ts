import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  public username: string = "";
  public password: string = "";
  public siging: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public signin(): void {
    this.authService.sigin(this.username, this.password)
      .subscribe((result) => {
        this.authService.saveToken(result.token);
        this.router.navigate(['/dashboard']);
      }, (error) => {
        alert('erro');
        alert(error.error);
      });
  }
}

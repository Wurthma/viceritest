export class Project {
    constructor(id: number,
        description: string,
        name: string,
        name_with_namespace: string,
        path: string,
        path_with_namespace: string,
        created_at: Date,
        import_status: string,
        IsImported: boolean) { }
}